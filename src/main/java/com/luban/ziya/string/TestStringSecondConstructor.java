package com.luban.ziya.string;

/**
 * Created By ziya
 * 2020/7/27
 */
public class TestStringSecondConstructor {

    public static void main(String[] args) {
        String s1 = new String(new char[]{'1', '1'}, 0, 2);

        String s2 = new String("22");
    }
}
